;; Die ersten drei Zeilen dieser Datei wurden von DrRacket eingefügt. Sie enthalten Metadaten
;; über die Sprachebene dieser Datei in einer Form, die DrRacket verarbeiten kann.
#reader(lib "DMdA-beginner-reader.ss" "deinprogramm")((modname Blatt1-A1) (read-case-sensitive #f) (teachpacks ((lib "image2.rkt" "teachpack" "deinprogramm"))) (deinprogramm-settings #(#f write repeating-decimal #f #t none explicit #f ((lib "image2.rkt" "teachpack" "deinprogramm")))))
; Die folgende Prozedur vergleicht zwei FKZs um ihren Wertunterschied und liefert
; #t, #f zurück, wenn der Unterschied innerhalb der angegeben Toleranz liegt.
(: equal-eps (real real real -> boolean))
(check-expect (equal-eps 1 1 1) #t)
(define equal-eps (lambda (a b eps) (< (abs (- a b)) eps)))
